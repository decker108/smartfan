#! /bin/python
import dht11_sensor
import sensor_repository as repo
import backend_service as backend
import tellstick_client
from multiprocessing import Pool
import time
import logging
import ping_service
from sys import exc_info

def setupLogging():
    logging.basicConfig(filename='status.log',level=logging.WARNING)
    logging.warning('Logging (re)started at %s', time.strftime("%d-%m-%Y %H:%M:%S"))

def sensorReadFailure(logs):
    return logs in [-1,-2,-3]

def last3LogsAreOverTempLimit(last3Logs):
    return len(filter(lambda x: x["temperature"] >= 23, last3Logs)) == 3

def isCurrentHourDaytime(hour):
    return hour > 9 and hour < 22
    
def sendLogsToServer():
    logging.info("Sending logs to server")
    backend.sendLogsToServer()

def shouldFanActivate():
    isDaytime = isCurrentHourDaytime(int(time.strftime('%H')))
    overTempLimit = last3LogsAreOverTempLimit(repo.getLastTrio())
    pingSuccess = True #ping_service.ping('daedalus.zyxel.com', 5)
    return all([overTempLimit, pingSuccess, isDaytime])

def main():
    setupLogging()

    pool = Pool(processes=1)
    while (True):
        try:
            for i in range(10):
                logs = dht11_sensor.readSensor()
                if sensorReadFailure(logs):
                    time.sleep(2)
                    continue
        
            if not sensorReadFailure(logs):
                # store sensor logs locally
                repo.storeData(logs['temp'], logs['humid'])
            
                # send logs to backend server
                pool.apply_async(sendLogsToServer, [])
                
                # make fan activation decision
                if shouldFanActivate():
                    tellstick_client.activateFan()
                else:
                    tellstick_client.deactivateFan()
                    
            if not isCurrentHourDaytime(int(time.strftime('%H'))):
                tellstick_client.deactivateFan()
            
            time.sleep(60)
        except:
            logging.error("%s %s",time.strftime("%d-%m-%Y %H:%M:%S"),exc_info())

if __name__ == "__main__":
    main()
