from os import system

def ping(hostname, retries):
    for i in range(retries):
        if system("ping -c 1 '" + hostname + "' >/dev/null 2>&1") == 0:
            return True
    return False
